---
title:  7. lekce - Hra v JavaScriptu
date:   2017-03-01
---

### Opáčko
- Teorie: [2. lekce]({{ "/lessons/2.html#javascript-a-terminologie" | relative_url }})
- Hra: <http://silentteacher.toxicode.fr/hourofcode>

### Programování hry
- Vyzkoušet JS v Chrome Dev Tools (Chrome menu -> Další nástroje -> Nástroje pro vývojáře nebo pravý klik na stránku -> Prozkoumat)
- [Šablona pro hru]({{ "/lessons/7-hra.html" | relative_url }})
- [Výsledek]({{ "/lessons/7-hra-vysledek.html" | relative_url }})

### Zdroje o JavaScriptu
- [Tahák k JS]({{ "/lessons/7-js-tahak.pdf" | relative_url }}) od Luďka Rolečka
- [JS na JanPsatWeb.cz](https://www.jakpsatweb.cz/javascript/)
